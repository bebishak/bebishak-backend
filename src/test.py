# General modules
from neo4j import GraphDatabase
from neo4j.exceptions import Neo4jError
from connectDB import neo4jDriver
from typing import Optional
from datetime import datetime, timedelta

# FastAPI modules
from fastapi import Depends, APIRouter, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm


# Create functions for NODES
# Some statements use MERGE in order to avoid the duplication of data, it will either create a new node OR find an existing node

def createUser(tx,id: str, name: str, username: str):
    with neo4jDriver.session() as session:
        result = session.run("MERGE (u:User{id:$id, name:$name, username: $username}) return u", 
        id=id, name=name, username=username)
        resultData = result.data()[0]
        print("result data:", resultData)
        print("name:", resultData['u']['name'])   

def createMealType(tx, mType:str):
    return tx.run("MERGE (m:MealType{name:$mType})", mType=mType)

def checkUser(userId:str):
    with neo4jDriver.session() as session:
        result = session.run("MATCH (u:User{id:$userId}) return u",userId=userId)
        
        if result.data():
            user = result.data()
            print (user)
            print("True") # true means that the user exists
            return True
        else:
            print (result.data())
            return False

    
def getLikedRecipes(username:str):
    with neo4jDriver.session() as session:
        # query tested
        results = session.run("MATCH (u:User{username:$username})-[:LIKES]->(r:Recipe) return r as recipes, u.username", username=username)
        # for loop
        recipesList = results.data()
        for i in range(len(recipesList)):
            recipe = recipesList[i]
            recipe_id = recipe['recipes']['id'] # take id of recipes
            
            results2 = getGlobalRating(id = recipe_id) # returning none, id 2 should return 4.5
            recipesList[i]['recipes']['globalRating'] = results2
            # return cuisine, meal types and ingredients tags
        
        # global rating of each recipe using each id
        
        print(recipesList)
        # return recipesList

def getGlobalRating(id:int):
    with neo4jDriver.session() as session:
        # query tested
        result = session.run("MATCH (r:Recipe{id:$id})<-[rate:RATED]-() RETURN avg(rate.rating)",id=id)

def createIngredient(tx, ingredientNameList): 
    return tx.run("UNWIND $ingredientNameList as ingredient MERGE (i:Ingredient{name:ingredient})", 
    ingredientNameList=ingredientNameList)
    

def createCuisineType(tx, cType:str): 
    return tx.run("MERGE (c:Cuisine{name:$cType})", cType=cType)


def createRecipe(tx, id: int, title:str, img:str, time: str, steps:str, desc:str, skill:int, isVegan:bool, username: str):
    # create/find the recipe then find user
    # id might change to auto indexed
    return tx.run("MATCH (u:User{username:$username}) with u MERGE (r:Recipe{id:$id, title:$title, img:$img, time:$time, steps:$steps, desc:$desc, skill:$skill, isVegan:$bool, date:date() MERGE (u)-[:WROTE]->(r)})",
    username=username, id=id, title=title, img=img, time=time, steps=steps, desc=desc, skill=skill, isVegan=isVegan)
    # havent added the connection between ingredients
    
def connectRecipe(tx, ingredientNameList):
    return tx.run("MATCH(r:Recipe {id:99}) WITH $ingredientNameList as ingredients, r UNWIND ingredients as ingredient MATCH (i:Ingredient{name:ingredient}) MERGE (r)-[:CONTAINS]->(i)",
    ingredientNameList=ingredientNameList)  

# Create functions for relationship between nodes
def recipeIngredients(tx, id:int, ingredientsList:list[list]): # needs revision, relationship properties isn't done yet
    # split nested list into 3 parts, nested array MUST be flattenedd or split in order for neo4j to work
    ingredientNameList, amountList, unitMeasureList = [], [], []
    for i in range(len(ingredientsList)):
        ingredientNameList.append(ingredientsList[i][0]) 
        amountList.append(ingredientsList[i][1])
        unitMeasureList.append(ingredientsList[i][2])
    return tx.run("UNWIND $ingredientsList as ingredients MERGE(i:Ingredient{name:ingredients[0]}) WITH ingredients,i MATCH (r:Recipe{id:$id}) WITH i, r, ingredients MERGE (r)-[:CONTAINS{amount:ingredients[1], unitMeasure:ingredients[2]}]->(i)", 
    ingredientsList=ingredientsList, id=id)

def recipeMeal(tx, id:int, meal:str):
    return tx.run("MATCH (r:Recipe {id:$id}) MATCH (mType:MealType {name:$meal}) WITH r, mType MERGE (r)-[:MTYPE]->(mType)", id=id, meal=meal)

def recipeCuisine(tx, id:int, cuisine:str):
    return tx.run("MATCH (r:Recipe {id:$id}) MATCH (cType:Cuisine{name:$cuisine}) WITH r, cType MERGE (r)-[:CTYPE]->(cType)", id=id, cuisine=cuisine)

def userLikesRecipe(tx, userID:str, recipeID:str):
    return tx.run("MATCH (u:User{id:$userID}) MATCH (r:Recipe {id:$id}) WITH u, r MERGE (u)-[:LIKES]->(r))", 
    userID=userID, id=recipeID)

def userRateRecipe(tx, userID:str, rate:float, recipeID:str):
    return tx.run("MATCH (u:User{id:$userID}) MATCH (r:Recipe {id:$id}) WITH u, r MERGE (u)-[:RATED{rating:$rate}]->(r)",
    userID=userID, id=recipeID, rate=rate)

def getRecipes(tx, name:str):
     with neo4jDriver.session() as session:
        
        result = session.run("MATCH (u:User{name:$name})-[l:LIKES]->(r:Recipe) return r", name=name )
        recipesList = result.data()
        print(recipesList)

# ingredientsList2 = [["Rum", 2, "teaspoon"],["Milk", 120, "millileter"]]
# testing functions 
# session.write_transaction(recipeIngredients,id=99, ingredientsList=ingredientsList2)
session = neo4jDriver.session()
# getLikedRecipes(username="user2")
# checkUser(userId="uid3")
getLikedRecipes(username="user3")
# session.write_transaction(createIngredient, ingredient="Flour")
# session.write_transaction(createCuisineType, cType="Korean")
# session.write_transaction(createMealType, mType="Brunch")
# session.write_transaction(recipeMeal, id=99, meal="Brunch")
# session.write_transaction(recipeCuisine, id=99, cuisine="Korean")
# session.write_transaction()
# session.write_transaction(userLikesRecipe)
# recipeIngredients(1,ingredientsList)
