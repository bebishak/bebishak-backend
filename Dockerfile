FROM python:3.10-slim

WORKDIR /bebishak-backend

COPY requirements.txt /bebishak-backend/requirements.txt

RUN pip install --upgrade pip
RUN pip install --no-cache-dir --upgrade -r /bebishak-backend/requirements.txt
RUN pip install --no-cache-dir requests

COPY ./src /bebishak-backend/src

WORKDIR /bebishak-backend/src

CMD ["uvicorn", "methods:app", "--host", "0.0.0.0", "--port", "80"]