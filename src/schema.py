from tokenize import Double
from typing import Optional, List
from pydantic import BaseModel, EmailStr
from datetime import datetime

# User response models

class UserOut(BaseModel):
    id:str = "testuserid"
    username:str = "testusername"
    notification: bool = False
    missingIngredients: bool = True

class UserId(BaseModel):
    id:str = "testuserid"
    search:Optional[str] = ""
    
# Recipe's response models and its related ones
class Cuisine(BaseModel):
    name:str = "Italian"

class Meal(BaseModel):
    name:str = "Breakfast"

class Ingredient(BaseModel):
    name:str = "Salt"

class globalRating(BaseModel):
    rating:Optional[float] = 1.00

class AvailCheck(BaseModel):
    ingredients:list = ["Rum", "Milk", "Egg"]
    search:Optional[str] = ""
    userId:str = "testuserid"

# Response models for queries that have relationships
class NodeBase(BaseModel):
    nodeID: int
    labels: list

class Node(NodeBase):
    properties: Optional[dict] = None

class Nodes(BaseModel):
    nodes: List[Node]

class UserToRecipe(BaseModel):
    rsID:int # not sure if this is needed rn
    rsType:str
    sourceNode:Node
    targetNode:Node
    properties:Optional[dict] = None

class Recipe(BaseModel):
    userId:str = "testuserid"
    title:str = "Nasi Goreng"
    img:str = ""
    time: int = 25
    steps:str = ""
    desc:str = "Nasi goreng recipe description"
    isVegan:bool = False
    ingredientsList:list[list] = [
        ["Rum", 2, "teaspoon"],
        ["Milk", 120, "millileter"]
    ]
    cuisines:list = ["Italian", "Indonesian", "American"]
    meals:list = ["Breakfast", "Lunch", "Dinner"]

class RecipeOut(BaseModel):
    title:str = "Nasi Goreng"
    img:str = "www.google.com"
    time: int = 25
    steps:str = ""
    desc:str = "Nasi goreng recipe description"
    isVegan:bool = False
    ingredientsList:list[list] = [
        ["Rum", 2, "teaspoon"],
        ["Milk", 120, "millileter"]
    ]
    cuisines:list = ["Italian", "Indonesian", "American"]
    meals:list = ["Breakfast", "Lunch", "Dinner"]
    rateCount:int = 22
    userRating:float = 4.67
    globalrates:Optional[globalRating] = globalRating(rating=0.00)
    isLiked:bool = False
    authorId:str = "authorid"
    authorUsername:str = "authorusername"


class LikesRecipe(BaseModel):
    authorId:str = "testuserid"
    likerId:str = "testuserid2"
    recipeTitle:str = "Nasi Goreng"

class RateRecipe(BaseModel):
    authorId:str = "testuserid"
    likerId:str = "testuserid2"
    recipeTitle:str = "Nasi Goreng"
    rating:float = 5.0