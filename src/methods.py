import requests
from http.client import HTTPException
from fastapi import FastAPI, status, UploadFile
# internal package
from connectDB import neo4jDriver
from schema import AvailCheck, Cuisine, RecipeOut, UserOut, LikesRecipe, Recipe, RateRecipe, UserId, globalRating
import os

# create user
app = FastAPI()
@app.post("/create", response_model=UserOut)
async def createUser(user:UserOut):
    with neo4jDriver.session() as session:
        # query tested
        result = session.run("MERGE (u:User{id:$id, username: $username, notification: $notif, missingIngredients: $missingIngredients}) return u", 
        id=user.id, username=user.username, notif=user.notification, missingIngredients=user.missingIngredients)
        userData = result.data()[0]['u']
        user = UserOut(**userData)
        return user

# check if user exists
@app.post("/check_user")
async def checkUser(userId: str):
    with neo4jDriver.session() as session:
        # query tested
        result = session.run("MATCH (u:User{id:$userId}) return u",userId=userId)
        if result.data():
            return True # user exists
        else:
            return False # user doesn't exist

# update user
@app.put("/{userId}/update", response_model=UserOut)
async def updateUser(updates:UserOut):

    with neo4jDriver.session() as session:
        # query tested
        query = "MATCH (u:User) WHERE u.id = $userId SET u.username = $username, u.notification = $notif, u.missingIngredients = $missingIngredients RETURN u"
        updatedUser = session.run(query,
        userId=updates.id, username=updates.username, notif=updates.notification, missingIngredients=updates.missingIngredients)
        userData = updatedUser.data()[0]['u']
        user = UserOut(**userData)
        return user


# create new recipe
@app.post("/new_recipe")
async def createRecipe(recipe:Recipe):
    with neo4jDriver.session() as session:

        # create recipe node first
        # should check recipe title isn't a copy of another recipe made by the same user
        searchRecipe = session.run("MATCH (u:User{id:$userId}) MATCH (r:Recipe{title:$title}) with u, r MATCH (u)-[:WROTE]-(r) return u, r", userId=recipe.userId,title=recipe.title)
        if searchRecipe.data():
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Recipe already exists with the same title, please change title.",
            headers={"WWW-Authenticate": "Bearer"})
            
        else:
            params = {"userId":recipe.userId,"title": recipe.title, "img":recipe.img, "time": recipe.time, "steps":recipe.steps, "desc":recipe.desc, "isVegan":recipe.isVegan}
            # query tested
            newRecipe = session.run("CREATE (r:Recipe{title:$title, img:$img, time:$time, steps:$steps, desc:$desc, isVegan:$isVegan}) WITH r MATCH (u:User{id:$userId}) WITH u, r MERGE (u)-[:WROTE{date:date()}]->(r) return r",
            params) # recipe is created and connected to the user who wrote it
            
            # take the ingredient list into nested list
            
            # query tested
            params = {"ingredientsList":recipe.ingredientsList, "userId":recipe.userId, "title":recipe.title}
            result = session.run("UNWIND $ingredientsList as ingredients MERGE(i:Ingredient{name:toUpper(ingredients[0])}) WITH ingredients,i MATCH (u:User{id:$userId})-[:WROTE]->(r:Recipe{title:$title}) WITH i, r, ingredients MERGE (r)-[:CONTAINS{amount:ingredients[1], unitMeasure:ingredients[2]}]->(i) return r", 
            params)

            # connect to types of cuisines and meals
            # query tested
            params={"userId":recipe.userId, "title":recipe.title, "cuisines":recipe.cuisines, "meals":recipe.meals}
            cuisineData = session.run("MATCH (u:User{id:$userId})-[:WROTE]->(r:Recipe{title:$title}) with r UNWIND $cuisines as cuisine MERGE (c:Cuisine{name:cuisine}) with c, r MERGE (r)-[:CTYPE]->(c)", params)
            mealsData = session.run("MATCH (u:User{id:$userId})-[:WROTE]->(r:Recipe{title:$title}) with r UNWIND $meals as meal MERGE (m:MealType{name:meal}) with m, r MERGE (r)-[:MTYPE]->(m)", params)
            recipeData = newRecipe.data()
            # recipeData = RecipeOut(title=recipeData[0]['title'])
            # return message success
            return newRecipe.data()

# get popular recipes of all time
@app.post("/popular_recipes", response_model=list[RecipeOut])
async def getPopularRecipe(userId:UserId):
    with neo4jDriver.session() as session:
        popularData = session.run("MATCH (u2:User)-[:WROTE]->(r:Recipe)-[:RATED]-() WITH u2,r\n"
        "MATCH (u2)-[:WROTE]->(r)<-[rate:RATED]-(u:User) WITH r, u2, rate, COUNT(*) as num order by num desc return distinct r, u2.id, u2.username, num")
        data = popularData.data()
        # get rating details of recipe
        recipeLists = []
        
        for i in range(len(data)):
            
            titles = data[i]['r']['title']
            imgs = data[i]['r']['img']
            times = data[i]['r']['time']
            stepsList = data[i]['r']['steps']
            descList = data[i]['r']['desc']
            isVegans = data[i]['r']['isVegan']   
            authorId = data[i]['u2.id']
            authorUsername = data[i]['u2.username']

            rating = LikesRecipe(authorId=authorId, likerId="",recipeTitle=titles)
            results2 = await getGlobalRating(rating)
            if results2 == False:
                results2 = globalRating(rating=0.00)
            
            userRating = session.run("MATCH (u:User{id:$userId})-[rate:RATED]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) return rate.rating as rate",
            userId=userId.id, title=titles, authorId=authorId)
            userRating = userRating.data()
            if userRating:
                userRating = userRating[0]['rate']
            else:
                userRating = 0.0
            
            ingredientListsData = session.run("MATCH (r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) WITH r MATCH (r)-[c:CONTAINS]->(i:Ingredient) RETURN i.name, c.amount, c.unitMeasure", 
            title=titles, authorId=authorId)
            ingredientListsData = ingredientListsData.data()

            ingredientLists = []
            for i in range(len(ingredientListsData)):
                ingredientLists.append([])
                
                ingredientLists[i].append(ingredientListsData[i]['i.name'])
                ingredientLists[i].append(ingredientListsData[i]['c.amount'])
                ingredientLists[i].append(ingredientListsData[i]['c.unitMeasure'])

            cuisineTags = session.run("MATCH (r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) WITH r MATCH (r)-[:CTYPE]->(c) RETURN c.name", 
            title=titles, authorId=authorId)
            cuisineTags = cuisineTags.data()
            cuisineTags2 = []
            for i in range(len(cuisineTags)):
                cuisineTags2.append(cuisineTags[i]['c.name'])

            mealTags = session.run("MATCH (r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) WITH r MATCH (r)-[:MTYPE]->(c) RETURN c.name", 
            title=titles, authorId=authorId)
            mealTags = mealTags.data()
            mealTags2 = []
            for i in range(len(mealTags)):
                mealTags2.append(mealTags[i]['c.name'])

            totalRates = session.run("MATCH (r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) WITH r MATCH (r)<-[rate:RATED]-() RETURN count(rate) as total", 
            title=titles, authorId=authorId)
            totalRates = totalRates.data()[0]['total']

            isLiked = session.run("MATCH (u:User{id:$userId})-[l:LIKES]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) RETURN l", 
            userId=userId.id, title=titles, authorId=authorId)
            isLiked = isLiked.data()
            
            if isLiked:
                isLiked = True
            else:
                isLiked = False
            
            recipe = RecipeOut(title=titles,img=imgs,time=times,steps=stepsList, desc=descList,ingredientsList=ingredientLists, 
            isVegan=isVegans,cuisines=cuisineTags2,meals=mealTags2,rateCount=totalRates,globalrates=results2,isLiked=isLiked, authorId=authorId,userRating=userRating, authorUsername=authorUsername)
            
            recipeLists.append(recipe)

        return recipeLists

# get trending recipe
@app.post("/get_trending_recipes", response_model=list[RecipeOut])
async def trendingRecipes(user:UserId):
    with neo4jDriver.session() as session:
        popularData = session.run("MATCH (u2:User)-[:WROTE]->(r:Recipe)-[:LIKES]-() WITH u2,r\n"
        "MATCH (u2)-[:WROTE]->(r)<-[rate:LIKES]-(u:User) WITH r, u2, rate, COUNT(*) as num order by num desc return distinct r, u2.id, u2.username, num")
        data = popularData.data()
        # get rating details of recipe
        recipeLists = []
        
        for i in range(len(data)):

            
            titles = data[i]['r']['title']
            imgs = data[i]['r']['img']
            times = data[i]['r']['time']
            stepsList = data[i]['r']['steps']
            descList = data[i]['r']['desc']
            isVegans = data[i]['r']['isVegan']   
            authorId = data[i]['u2.id']
            authorUsername = data[i]['u2.username']

            rating = LikesRecipe(authorId=authorId, likerId="",recipeTitle=titles)
            results2 = await getGlobalRating(rating)
            if results2 == False:
                results2 = globalRating(rating=0.00)
            
            userRating = session.run("MATCH (u:User{id:$userId})-[rate:RATED]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) return rate.rating as rate",
            userId=user.id, title=titles, authorId=authorId)
            userRating = userRating.data()
            if userRating:
                userRating = userRating[0]['rate']
            else:
                userRating = 0.0
            
            ingredientListsData = session.run("MATCH (r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) WITH r MATCH (r)-[c:CONTAINS]->(i:Ingredient) RETURN i.name, c.amount, c.unitMeasure", 
            title=titles, authorId=authorId)
            ingredientListsData = ingredientListsData.data()

            ingredientLists = []
            for i in range(len(ingredientListsData)):
                ingredientLists.append([])
                
                ingredientLists[i].append(ingredientListsData[i]['i.name'])
                ingredientLists[i].append(ingredientListsData[i]['c.amount'])
                ingredientLists[i].append(ingredientListsData[i]['c.unitMeasure'])

            cuisineTags = session.run("MATCH (r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) WITH r MATCH (r)-[:CTYPE]->(c) RETURN c.name", 
            title=titles, authorId=authorId)
            cuisineTags = cuisineTags.data()
            cuisineTags2 = []
            for i in range(len(cuisineTags)):
                cuisineTags2.append(cuisineTags[i]['c.name'])

            mealTags = session.run("MATCH (r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) WITH r MATCH (r)-[:MTYPE]->(c) RETURN c.name", 
            title=titles, authorId=authorId)
            mealTags = mealTags.data()
            mealTags2 = []
            for i in range(len(mealTags)):
                mealTags2.append(mealTags[i]['c.name'])

            totalRates = session.run("MATCH (r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) WITH r MATCH (r)<-[rate:RATED]-() RETURN count(rate) as total", 
            title=titles, authorId=authorId)
            totalRates = totalRates.data()[0]['total']

            isLiked = session.run("MATCH (u:User{id:$userId})-[l:LIKES]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) RETURN l", 
            userId=user.id, title=titles, authorId=authorId)
            isLiked = isLiked.data()
            
            if isLiked:
                isLiked = True
            else:
                isLiked = False
            
            recipe = RecipeOut(title=titles,img=imgs,time=times,steps=stepsList, desc=descList,ingredientsList=ingredientLists, 
            isVegan=isVegans,cuisines=cuisineTags2,meals=mealTags2,rateCount=totalRates,globalrates=results2,isLiked=isLiked, authorId=authorId,userRating=userRating, authorUsername=authorUsername)
            
            recipeLists.append(recipe)

        return recipeLists
    

# update recipe
@app.put("/update_recipe",response_model=Recipe)
async def updateRecipe(recipe:Recipe):
    with neo4jDriver.session() as session:
        params = {} # what if we delete then make a new recipe
        deleteData = session.run("MATCH (r:Recipe{title:$title})<-[:WROTE]-(u:User{id:$userId}) WITH r DETACH DELETE r", title=recipe.title, userId=recipe.userId)
        newRecipe = await createRecipe(recipe)
        return recipe

# delete recipe
@app.post("/delete_recipe")
async def deleteRecipe(title:str, userId:str):
    with neo4jDriver.session() as session:
        # query tested
        result = session.run("MATCH (u:User{id:$userId})-[:WROTE]->(r:Recipe{title:$title}) DETACH DELETE r", title=title, userId=userId)
        if result.data() == False:
            return "Successfuly deleted"

# get user's recipes
@app.post("/get_own_recipe", response_model=list[RecipeOut])
async def getOwnRecipe(userId:UserId):
    with neo4jDriver.session() as session:
        # query tested
        results = session.run("MATCH (u:User{id:$userId})-[:WROTE]->(r:Recipe) return r", userId=userId.id)
        # returns a list of recipes the user has written
        data = results.data()
        # find username
        usernameData = session.run("MATCH (u:User{id:$userId}) return u.username", userId=userId.id)
        usernameData = usernameData.data()
        username = usernameData[0]['u.username']
        
        # get rating details of recipe
        recipeLists = []
        for i in range(len(data)):

            titles = data[i]['r']['title']
            imgs = data[i]['r']['img']
            times = data[i]['r']['time']
            stepsList = data[i]['r']['steps']
            descList = data[i]['r']['desc']
            isVegans = data[i]['r']['isVegan']

            rating = LikesRecipe(authorId=userId.id, likerId="",recipeTitle=titles)
            results2 = await getGlobalRating(rating)
            if results2 == False:
                results2 = globalRating(rating=0.00)

            userRating = session.run("MATCH (u:User{id:$userId})-[rate:RATED]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$userId}) return rate.rating as rate",
            userId=userId.id, title=titles)
            userRating = userRating.data()
            if userRating:
                userRating = userRating[0]['rate']
            else:
                userRating = 0.0

            ingredientListsData = session.run("MATCH (u:User{id:$userId})-[:WROTE]->(r:Recipe{title:$title})-[c:CONTAINS]->(i:Ingredient) RETURN i.name, c.amount, c.unitMeasure", 
            userId=userId.id, title=titles)
            ingredientListsData = ingredientListsData.data()
        
            ingredientLists = []
            for i in range(len(ingredientListsData)):
                ingredientLists.append([])
                
                ingredientLists[i].append(ingredientListsData[i]['i.name'])
                ingredientLists[i].append(ingredientListsData[i]['c.amount'])
                ingredientLists[i].append(ingredientListsData[i]['c.unitMeasure'])

            cuisineTags = session.run("MATCH (u:User{id:$userId})-[:WROTE]->(r:Recipe{title:$title})-[:CTYPE]->(c) RETURN c.name", userId=userId.id, title=titles)
            cuisineTags = cuisineTags.data()
            cuisineTags2 = []
            for i in range(len(cuisineTags)):
                cuisineTags2.append(cuisineTags[i]['c.name'])

            mealTags = session.run("MATCH (u:User{id:$userId})-[:WROTE]->(r:Recipe{title:$title})-[:MTYPE]->(c) RETURN c.name", userId=userId.id, title=titles)
            mealTags = mealTags.data()
            mealTags2 = []
            for i in range(len(mealTags)):
                mealTags2.append(mealTags[i]['c.name'])

            totalRates = session.run("MATCH (u:User{id:$userId})-[:WROTE]->(r:Recipe{title:$title})<-[rate:RATED]-() RETURN count(rate) as total", userId=userId.id, title=titles)
            totalRates = totalRates.data()[0]['total']

            isLiked = session.run("MATCH (u:User{id:$userId})-[:WROTE]->(r:Recipe{title:$title})<-[l:LIKES]-(u2:User{id:$userId}) RETURN l", userId=userId.id, title=titles)
            isLiked = isLiked.data()
            
            if isLiked:
                isLiked = True
            else:
                isLiked = False
            
            recipe = RecipeOut(title=titles,img=imgs,time=times,steps=stepsList,
            desc=descList,ingredientsList=ingredientLists, isVegan=isVegans,cuisines=cuisineTags2,meals=mealTags2,rateCount=totalRates,globalrates=results2,isLiked=isLiked, userRating=userRating, authorId=userId.id, authorUsername=username)
            
            if userId.search == "":
                recipeLists.append(recipe)
            else:
                # titles = str(data[i]['r']['title'])
                if titles.find(userId.search) != -1:
                    recipeLists.append(recipe)
                else:
                    pass

        return recipeLists

# get recipes liked by the user
@app.post("/get_liked_recipes", response_model=list[RecipeOut])
async def getLikedRecipes(userId:UserId):
    with neo4jDriver.session() as session:
        # query tested
        results = session.run("MATCH (u:User{id:$userId})-[:LIKES]->(r:Recipe) return r", userId=userId.id)
        data = results.data()

        # get rating details of recipe
        recipeLists = []
        
        for i in range(len(data)):

            
            titles = data[i]['r']['title']
            imgs = data[i]['r']['img']
            times = data[i]['r']['time']
            stepsList = data[i]['r']['steps']
            descList = data[i]['r']['desc']
            isVegans = data[i]['r']['isVegan']
            authorData = session.run("MATCH (u:User{id:$userId})-[:LIKES]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User) return u2.id, u2.username", userId=userId.id, title=titles)
            authorData = authorData.data()
            authorId, authorUsername = "", ""
            for i in range(len(authorData)):
                authorId = authorData[i]['u2.id']
                authorUsername = authorData[i]['u2.username']

            rating = LikesRecipe(authorId=authorId, likerId="",recipeTitle=titles)
            results2 = await getGlobalRating(rating)
            if results2 == False:
                results2 = globalRating(rating=0.00)

            userRating = session.run("MATCH (u:User{id:$userId})-[rate:RATED]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) return rate.rating as rate",
            userId=userId.id, title=titles, authorId=authorId)
            userRating = userRating.data()
            if userRating:
                userRating = userRating[0]['rate']
            else:
                userRating = 0.0
            
            ingredientListsData = session.run("MATCH (u:User{id:$userId})-[:LIKES]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) WITH r MATCH (r)-[c:CONTAINS]->(i:Ingredient) RETURN i.name, c.amount, c.unitMeasure", 
            userId=userId.id, title=titles, authorId=authorId)
            ingredientListsData = ingredientListsData.data()

            ingredientLists = []
            for i in range(len(ingredientListsData)):
                ingredientLists.append([])
                
                ingredientLists[i].append(ingredientListsData[i]['i.name'])
                ingredientLists[i].append(ingredientListsData[i]['c.amount'])
                ingredientLists[i].append(ingredientListsData[i]['c.unitMeasure'])

            cuisineTags = session.run("MATCH (u:User{id:$userId})-[:LIKES]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) WITH r MATCH (r)-[:CTYPE]->(c) RETURN c.name", 
            userId=userId.id, title=titles, authorId=authorId)
            cuisineTags = cuisineTags.data()
            cuisineTags2 = []
            for i in range(len(cuisineTags)):
                cuisineTags2.append(cuisineTags[i]['c.name'])

            mealTags = session.run("MATCH (u:User{id:$userId})-[:LIKES]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) WITH r MATCH (r)-[:MTYPE]->(c) RETURN c.name", 
            userId=userId.id, title=titles, authorId=authorId)
            mealTags = mealTags.data()
            mealTags2 = []
            for i in range(len(mealTags)):
                mealTags2.append(mealTags[i]['c.name'])

            totalRates = session.run("MATCH (u:User{id:$userId})-[:LIKES]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) WITH r MATCH (r)<-[rate:RATED]-() RETURN count(rate) as total", 
            userId=userId.id, title=titles, authorId=authorId)
            totalRates = totalRates.data()[0]['total']

            isLiked = session.run("MATCH (u:User{id:$userId})-[l:LIKES]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) RETURN l", 
            userId=userId.id, title=titles, authorId=authorId)
            isLiked = isLiked.data()
            
            if isLiked:
                isLiked = True
            else:
                isLiked = False
            
            recipe = RecipeOut(title=titles,img=imgs,time=times,steps=stepsList, desc=descList,ingredientsList=ingredientLists, 
            isVegan=isVegans,cuisines=cuisineTags2,meals=mealTags2,rateCount=totalRates,globalrates=results2,isLiked=isLiked,userRating=userRating, authorId=authorId, authorUsername=authorUsername)
            
            if userId.search == "":
                recipeLists.append(recipe)
            else:
                # titles = str(data[i]['r']['title'])
                if titles.find(userId.search) != -1:
                    recipeLists.append(recipe)
                else:
                    pass

        return recipeLists

# like a new recipe
@app.post("/likes_recipe")
async def likesRecipe(likes:LikesRecipe):
    with neo4jDriver.session() as session:
        params = {"userId":likes.likerId, "authorId":likes.authorId, "title":likes.recipeTitle}
        # query tested
        result = session.run("MATCH (u:User{id:$userId}) WITH u\n"
        "MATCH (u2:User{id:$authorId})-[:WROTE]->(r:Recipe {title:$title}) WITH u, r\n"
        "MERGE (u)-[:LIKES]->(r) return r", params)
        if result.data():
            return "recipe has been liked"
    
# unlike a recipe
@app.post("/unlike_recipe")
async def unlikeRecipe(likes:LikesRecipe):
    with neo4jDriver.session() as session: 
        params = {"userId":likes.likerId, "authorId":likes.authorId, "title":likes.recipeTitle}
        # query tested
        result = session.run("MATCH (u:User{id:$userId})-[l:LIKES]->(r:Recipe {title:$title})<-[:WROTE]-(u2:User{id:$authorId}) DELETE l", params)
        if result.data() == False:
            return "Successfuly deleted"

# return recipes based on available ingredients and several conditions
@app.post("/get_recipes_avail", response_model=list[RecipeOut])
async def getRecipesAvail(available:AvailCheck):
    with neo4jDriver.session() as session:
        
        # query tested 
        result = session.run("UNWIND $ingredients as ingredient MATCH (i:Ingredient{name:ingredient})<-[:CONTAINS]-(r:Recipe)<-[:WROTE]-(u:User) RETURN DISTINCT r, u.id, u.username", 
        ingredients=available.ingredients)
        data = result.data()

        # get rating details of recipe
        recipeLists = []
        recipesTitles = []
        for i in range(len(data)):

            titles = data[i]['r']['title']
            imgs = data[i]['r']['img']
            times = data[i]['r']['time']
            stepsList = data[i]['r']['steps']
            descList = data[i]['r']['desc']
            isVegans = data[i]['r']['isVegan']
            authorId = data[i]['u.id']
            authorUsername = data[i]['u.username'] 
            recipesTitles.append({})
            recipesTitles[i]['title'] = titles
            recipesTitles[i]['authorId'] = authorId 
            recipesTitles[i]['authorUsername'] = authorUsername   

            rating = LikesRecipe(authorId=authorId, likerId="",recipeTitle=titles)
            results2 = await getGlobalRating(rating)
            if results2 == False:
                results2 = globalRating(rating=0.00)
            
            ingredientListsData = session.run("MATCH (u:User{id:$authorId})-[:WROTE]->(r:Recipe{title:$title})-[c:CONTAINS]->(i:Ingredient) RETURN i.name, c.amount, c.unitMeasure", 
            authorId=authorId, title=titles)
            ingredientListsData = ingredientListsData.data()

            authorData = session.run("MATCH (r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) return u2.id, u2.username", authorId=authorId, title=titles)
            authorData = authorData.data()
            authorId, authorUsername = "", ""
            for i in range(len(authorData)):
                authorId = authorData[i]['u2.id']
                authorUsername = authorData[i]['u2.username']
        

            ingredientLists = []
            for i in range(len(ingredientListsData)):
                ingredientLists.append([])
                
                ingredientLists[i].append(ingredientListsData[i]['i.name'])
                ingredientLists[i].append(ingredientListsData[i]['c.amount'])
                ingredientLists[i].append(ingredientListsData[i]['c.unitMeasure'])

            cuisineTags = session.run("MATCH (u:User{id:$authorId})-[:WROTE]->(r:Recipe{title:$title})-[:CTYPE]->(c) RETURN c.name", authorId=authorId, title=titles)
            cuisineTags = cuisineTags.data()
            cuisineTags2 = []
            for i in range(len(cuisineTags)):
                cuisineTags2.append(cuisineTags[i]['c.name'])

            mealTags = session.run("MATCH (u:User{id:$authorId})-[:WROTE]->(r:Recipe{title:$title})-[:MTYPE]->(c) RETURN c.name", authorId=authorId, title=titles)
            mealTags = mealTags.data()
            mealTags2 = []
            for i in range(len(mealTags)):
                mealTags2.append(mealTags[i]['c.name'])

            totalRates = session.run("MATCH (u:User{id:$authorId})-[:WROTE]->(r:Recipe{title:$title})<-[rate:RATED]-() RETURN count(rate) as total", authorId=authorId, title=titles)
            totalRates = totalRates.data()[0]['total']

            isLiked = session.run("MATCH (u:User{id:$userId})-[l:LIKES]->(r:Recipe{title:$title}) RETURN l", userId=available.userId, title=titles)
            isLiked = isLiked.data()

            userRating = session.run("MATCH (u:User{id:$userId})-[rate:RATED]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) return rate.rating as rate",
            userId=available.userId, title=titles, authorId=authorId)
            userRating = userRating.data()
            if userRating:
                userRating = userRating[0]['rate']
            else:
                userRating = 0.0
            
            if isLiked:
                isLiked = True
            else:
                isLiked = False
            recipe = RecipeOut(title=titles,img=imgs,time=times,steps=stepsList,desc=descList,ingredientsList=ingredientLists, 
            isVegan=isVegans,cuisines=cuisineTags2,meals=mealTags2,rateCount=totalRates,globalrates=results2,isLiked=isLiked,userRating=userRating, authorId=authorId, authorUsername=authorUsername)

            if available.search == "":
                recipeLists.append(recipe)
            else:
                # titles = str(data[i]['r']['title'])
                if titles.find(available.search) != -1:
                    recipeLists.append(recipe)
                else:
                    pass

        return recipeLists
        

# recipe's global rating
@app.post("/globalrating", response_model=globalRating)
async def getGlobalRating(rating:LikesRecipe):
    with neo4jDriver.session() as session:
        # query tested
        params = {"authorId":rating.authorId, "title":rating.recipeTitle}
        result = session.run("MATCH (u2:User{id:$authorId})-[:WROTE]->(r:Recipe{title:$title})<-[rate:RATED]-() RETURN avg(rate.rating) as rate", params)

        return globalRating(rating=result.data()[0]['rate'])
        

# rating recipes and update
@app.post("/rate_recipe", status_code=status.HTTP_201_CREATED)
async def rateRecipe(rating:RateRecipe):
    with neo4jDriver.session() as session:

        # check if rating exists
        params = {"userId":rating.likerId, "authorId":rating.authorId, "title":rating.recipeTitle, "rating":rating.rating}
        check = session.run("MATCH (u:User{id:$userId})-[rate:RATED]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) return rate", params)
        check = check.data()
    
        # query tested
        if check:
            result = session.run("MATCH (u:User{id:$userId})-[rate:RATED]->(r:Recipe{title:$title})<-[:WROTE]-(u2:User{id:$authorId}) with rate SET rate.rating = $rating return rate.rating", params)
            if result.data():
                return {"status_code": "201", "status_message" : "Success"}
        else:
            result = session.run("MATCH (u:User{id:$userId}) WITH u MATCH (u2:User{id:$authorId})-[:WROTE]->(r:Recipe{title:$title}) WITH u, r, u2 MERGE (u)-[r2:RATED{rating:$rating}]->(r) return r2.rating",
            params)
            if result.data():
                return {"status_code": "201", "status_message" : "Success"}


# create cuisine
@app.post("/create_cuisine", response_model=Cuisine)
async def createCuisineType(cuisine:str): 
    with neo4jDriver.session() as session:
        # query tested
        result = session.run("MERGE (c:Cuisine{name:$cuisine}) return c.name as name",cuisine=cuisine)
        cu = result.data()[0]['name']
        return Cuisine(name=cu)

# Send image to ai
@app.post("/detect-ingredients")
async def detect_ingredients(file: UploadFile):

    url = os.environ.get('AI_ADDR')
    response = requests.post(url, files={"file" : file.file})

    
    detected_ingredients = response.json()['result']

    return {
        "ingredients" : detected_ingredients
    }
    