
from neo4j import GraphDatabase
from neo4j.exceptions import Neo4jError
import os 
from dotenv import load_dotenv, find_dotenv
env_loc = find_dotenv('.env')
load_dotenv(env_loc)

# create driver instance
uri = os.environ.get('NEO4J_URI')
username = os.environ.get('NEO4J_USERNAME')
password = os.environ.get('NEO4J_PASSWORD')
neo4jDriver = GraphDatabase.driver(uri, auth=(username, password))